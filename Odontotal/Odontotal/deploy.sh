
# Copiar el archivo JAR al servidor EC2
scp -o "StrictHostKeyChecking no" Odontotal/Odontotal/target/Odontotal-0.0.1-SNAPSHOT.jar ubuntu@ec2-34-227-29-15.compute-1.amazonaws.com:/home/ubuntu

# Conectarse al servidor EC2 y ejecutar el nuevo JAR
ssh -o "StrictHostKeyChecking no" ubuntu@ec2-34-227-29-15.compute-1.amazonaws.com "sudo systemctl stop odontotal && sudo systemctl start odontotal
"
