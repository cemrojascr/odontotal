import React from 'react'
import NavbarPatient from '../../components/componentPatient/NavbarPatient'
import Slider from '../../components/componentPatient/Slider'

const Home = () => {
  return (
    <div>
      <NavbarPatient/>
      Home
      <Slider></Slider></div>
  )
}

export default Home